# 人工智能相关课程：课件 Slides

> - [高飞](http://aiart.live) @ 杭州电子科技大学，计算机学院
> - 个人教学课件，参考了很多网络资源。

【[主页](https://aiart.live/courses/)】

| <img src="/imgs/mldl.jpg" height="150">  |  <img src="/imgs/dl.jpg" height="150">   |  <img src="/imgs/ai.jpg" height="150">   |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
| [机器学习与深度学习](https://aiart.live/courses/mldl.html) | [深度学习](https://aiart.live/courses/dl.html) | [人工智能导论](https://aiart.live/courses/i2ai.html) |
|  <img src="/imgs/cvf.jpg" height="150">  |  <img src="/imgs/cv.jpg" height="150">   |  <img src="/imgs/dip.jpg" height="150">  |
| [计算机视觉基础](https://aiart.live/courses/cvf.html) | [计算机视觉](https://aiart.live/courses/cv.html) | [数字图像处理](https://aiart.live/courses/dip.html) |


> - 有些页面还在更新过程中...
> - 如有谬误，欢迎指出